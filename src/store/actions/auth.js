import axios from 'axios';

import * as actionTypes from './actionTypes';

const loginRequest = (email, password) => {
    const authData = {
        email: email,
        password: password,
        returnSecureToken: true
    };
    let url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyBfrn18YUOFviWt3ajpr3ENLR0jzPryiEY';
    return axios.post(url, authData);
};

export const login = (email, password) => {
    return dispatch => {
        dispatch(loginStart());
        loginRequest(email, password)
            .then(response => {
                console.log(response);
                dispatch(loginSuccess(response.data.idToken, response.data.localId));
            }).catch(error => {
                console.log(error);
                dispatch(loginFail(error.response.data.error))
            });
    };
};

export const loginStart = () => {
    return {
        type: actionTypes.LOGIN_START
    };
};

export const loginSuccess = (token, userId) => {
    return {
        type: actionTypes.LOGIN_SUCCESS,
        token: token,
        userId: userId
    };
};

export const loginFail = (error) => {
    return {
        type: actionTypes.LOGIN_FAIL,
        error: error
    };
};

