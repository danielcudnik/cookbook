import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Layout from './containers/Layout/Layout';
import Auth from './containers/Auth/Auth';
import Home from './containers/Home/Home';

import './App.scss';
import asyncComponent from './hoc/asyncComponent/asyncComponent';

const asyncRecipes = asyncComponent(() => {
  return import('./containers/Recipes/Recipes');
});

const asyncNewRecipe = asyncComponent(() => {
  return import('./containers/NewRecipe/NewRecipe');
});

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/add-recipe" component={asyncNewRecipe} />
          <Route path="/recipies" component={asyncRecipes} />
          <Route path="/login" component={Auth} />
          <Route path="/" exact component={Home} />
          <Redirect to="/" />
        </Switch>
      </Layout>
    );
  }
}

export default App;