import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://cookbook-964e4.firebaseio.com'
});

export default instance;