import React from 'react';

import ChefHat from '../../assets/images/chef-toque-and-mustache.png';

const logo = () => (
    <img src={ChefHat} alt='logo'/>
);

export default logo;