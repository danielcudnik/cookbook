import React from 'react';

import './Modal.scss';

const modal = (props) => (
    <div
        style={{
            transform: props.isOpen ? 'translateY(0)' : 'translateY(-100vh)',
            opacity: props.isOpen ? '1' : '0'
        }}
        className="modal-dialog modal-dialog-centered Modal"
        role="document">
        <div className="modal-content">
            {props.children}
        </div>
    </div>
);

export default modal;