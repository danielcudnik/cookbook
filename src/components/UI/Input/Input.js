import React from 'react';

const input = (props) => {
    return (
        <input
            className={props.classes}
            type={props.type}
            placeholder={props.placeholder}
            value={props.value}
            onChange={props.changed}
        />
    );
}
export default input;