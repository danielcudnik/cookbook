import React from 'react';

import IngredientsContainer from './Ingredients/IngredientsContainer/IngredientsContainer';
import Button from '../UI/Button/Button';

import './RecipeForm.scss';

const recipe = (props) => (
    <form className="RecipeForm" onSubmit={props.submitted}>
        <div className="form-group">
            <label>Title</label>
            <input type="text" className="form-control" placeholder="Type title" />
        </div>
        <div className="form-group">
            <label>Ingredients</label>
            <IngredientsContainer
                ingredients={props.ingredients}
                removeIngredient={props.removeIngredient}
            />
            <Button clicked={props.openAddIngredientModal} classes="btn btn-outline-secondary Button__RecipeForm">Add Ingredient</Button>
        </div>
        <div className="form-group">
            <div className="row">
                <div className="col-2">
                    Add ingredient:
                </div>
                <div className="col-10">
                    One of three columns
                </div>
            </div>
        </div>
        <div className="form-group">
            <label>Example select</label>
            <select className="form-control">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </select>
        </div>
        <div className="form-group">
            <label>Example multiple select</label>
            <select multiple className="form-control">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </select>
        </div>
        <div className="form-group">
            <label>Directions</label>
            <textarea className="form-control" rows="3"></textarea>
        </div>
    </form>
);

export default recipe;