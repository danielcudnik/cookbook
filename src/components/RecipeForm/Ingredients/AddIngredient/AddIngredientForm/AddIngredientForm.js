import React from 'react';

import './AddIngredientForm.scss';

const addIngredientForm = (props) => (
    <form className="AddIngredientForm">
        <h3>Add Ingredient</h3>
        <div className="form-group">
            <label>Name</label>
            <input type="text" className="form-control" id="exampleFormControlInput1" placeholder="name@example.com" />
        </div>
        <div className="form-group">
            <label>Quantity</label>
            <div className="row">
                <div className="col-8">
                    <input type="number" className="form-control" id="exampleFormControlInput1" placeholder="name@example.com" />
                </div>
                <div className="col-4">
                    <select className="form-control" id="exampleFormControlSelect1">
                        <option>psc</option>
                        <option>ml</option>
                        <option>g</option>
                    </select>
                </div>
            </div>
        </div>
        <div className="form-group">
            <label>Calories</label>
            <div className="row">
                <div className="col-8">
                    <input type="number" className="form-control" id="exampleFormControlInput1" placeholder="name@example.com" />
                </div>
                <div className="col-4">
                    <label>kcal</label>
                </div>
            </div>
        </div>
        <div className="form-group">
            <button>sss</button>
        </div>
    </form>
);

export default addIngredientForm;