import React from 'react';

import Modal from '../../../UI/Modal/Modal';
import Backdrop from '../../../UI/Backdrop/Backdrop';
import AddIngredientForm from './AddIngredientForm/AddIngredientForm';
import Aux from '../../../../hoc/Auxiliary/Auxiliary';

const addIngredient = (props) => (
    <Aux>
        <Backdrop clicked={props.onCancel} show={props.show}/>
        <Modal isOpen={props.show}>
            <AddIngredientForm />
        </Modal>
    </Aux>
);

export default addIngredient;