import React from 'react';

import './IngredientItem.scss';

const ingredientItem = (props) => (
    <div onClick={props.clicked} className="IngredientItem">
        <span>{props.children}</span>
    </div>
);

export default ingredientItem;