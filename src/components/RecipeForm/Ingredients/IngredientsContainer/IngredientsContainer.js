import React from 'react';

import IngredientItem from './IngredientItem/IngredientItem';
import './IngredientsContainer.scss';

const ingredientsContainer = (props) => {
    let ingredients = <span>Add some ingredients</span>;

    if (props.ingredients.length > 0) {
        ingredients = props.ingredients.map(ingredient => {
            return (
                <IngredientItem
                    key={ingredient.id}
                    clicked={() => props.removeIngredient(ingredient.id)}>
                    {ingredient.name}: {ingredient.quantity}{ingredient.quantityType}
                </IngredientItem>
            )
        });
    }
    return (
        <div className="IngredientsContainer">
            {ingredients}
        </div>
    );
}

export default ingredientsContainer;