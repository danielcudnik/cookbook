import React from 'react';

import Button from '../../UI/Button/Button';
import Input from '../../UI/Input/Input';

const searchBar = (props) => {
    return (
        <form className="form-inline my-2 my-lg-0">
            <Input 
                classes="form-control mr-sm-2" type="search" placeholder="Search"
            />
            <Button classes="btn btn-outline-success my-2 my-sm-0">Search</Button>
        </form>
    );
}
export default searchBar;