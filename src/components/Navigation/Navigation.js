import React from 'react';

import NavItems from './NavItems/NavItems';
import Button from '../UI/Button/Button';
import SearchBar from './SearchBar/SearchBar';
import Logo from '../Logo/Logo';

const navigation = (props) => {
    let collapseClasses = ['collapse', 'navbar-collapse'];
    if (props.collapseNavActive) {
        collapseClasses.push('show');
    }
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <Logo />
            <a className="navbar-brand" href="/">Cookbook</a>
            <Button classes="navbar-toggler" clicked={props.toggleMenu}><span className="navbar-toggler-icon"></span></Button>
            <div className={collapseClasses.join(' ')} id="navbarSupportedContent">
                <NavItems isAuthenticated={props.isAuthenticated} />
                <SearchBar />
            </div>
        </nav>
    );
}

export default navigation;