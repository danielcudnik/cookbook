import React from 'react';

import NavItem from './NavItem/NavItem';

const navItems = (props) => {
    return (
        <ul className="navbar-nav mr-auto">
            <NavItem link="/" exact>Home</NavItem>
            <NavItem link="/recipies">Recipes</NavItem>
            <NavItem link="/add-recipe">Add Recipe</NavItem>
            {!props.isAuthenticated ? <NavItem link="/login">Login</NavItem> : null}
        </ul>
    );
}
export default navItems;