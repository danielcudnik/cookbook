import React from 'react';

import { NavLink } from 'react-router-dom';

const navItem = (props) => {
    let classes = ['nav-link'];
    if (props.additionalClasses) {
        classes.push(props.additionalClasses);
    }
    return (
        <li className="nav-item">
            <NavLink
                exact={props.exact}
                className={classes.join(' ')}
                to={props.link}>{props.children}</NavLink>
        </li>
    );
}
export default navItem;