import React, { Component } from 'react';

import { Ingredient } from '../../shared/objectsConstructors';
import RecipeForm from '../../components/RecipeForm/RecipeForm';
import AddIngredient from '../../components/RecipeForm/Ingredients/AddIngredient/AddIngredient';
import './NewRecipe.scss';

class NewRecipe extends Component {
    state = {
        title: '',
        ingredients: [],
        directions: '',
        notes: '',
        calories: 0,
        showModal: false
    }

    componentDidMount() {
        let ingredients = [...this.state.ingredients];
        ingredients.push(new Ingredient('Salad', 6, 'szt', 200));
        console.log(ingredients);
        this.setState({ ingredients: ingredients });
    }

    removeIngredient = (id) => {
        console.log(id);
        const ingredients = this.state.ingredients.filter(ingredient => {
            return ingredient.id !== id;
        });
        this.setState({ ingredients: ingredients });
    };

    toggleAddIngredient = (event) => {
        event.preventDefault();
        this.setState(prevState => {
            return {showModal: !prevState.showModal};
        });
    };

    render() {
        return (
            <div className="NewRecipe">
                <h1>CREATE NEW RECIPE</h1>
                <RecipeForm
                    openAddIngredientModal={this.toggleAddIngredient}
                    ingredients={this.state.ingredients}
                    removeIngredient={this.removeIngredient}
                />
                <AddIngredient show={this.state.showModal} onCancel={this.toggleAddIngredient} />
            </div>
        );
    }
}

export default NewRecipe;
