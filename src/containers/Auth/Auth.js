import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { login } from '../../store/actions/index';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import Aux from '../../hoc/Auxiliary/Auxiliary';

import './Auth.scss';

class Auth extends Component {
    state = {
        email: '',
        password: ''
    }

    inputChangeHandler = (event, controlName) => {
        this.setState({ [controlName]: event.target.value });
    };

    submitHandler = (event) => {
        event.preventDefault();
        this.props.onLoginHandler(this.state.email, this.state.password);
    };

    render() {
        let redirect = null;

        if (this.props.isAuthenticated) {
            redirect = <Redirect to="/" />
        }
        return (
            <Aux>
                {redirect}
                <form onSubmit={this.submitHandler} className="justify-content-center align-items-center Auth border">
                    <div className="form-group">
                        <label>Email address</label>
                        <Input
                            type="email"
                            classes="form-control"
                            placeholder="Enter email"
                            value={this.state.email}
                            changed={(event) => this.inputChangeHandler(event, 'email')}
                        />
                        <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <Input
                            type="password"
                            classes="form-control"
                            placeholder="Password"
                            value={this.state.password}
                            changed={(event) => this.inputChangeHandler(event, 'password')}
                        />
                    </div>
                    <Button classes="btn btn-primary">Log In</Button>
                </form>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onLoginHandler: (email, password) => dispatch(login(email, password))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
