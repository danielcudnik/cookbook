import React, { Component } from 'react';
import { connect } from 'react-redux';

import Aux from '../../hoc/Auxiliary/Auxiliary';
import Navigation from '../../components/Navigation/Navigation';

class Layout extends Component {
    state = {
        collapseNavActive: false
    }

    toggleMenu = () => {
        this.setState(prevState => {
            return { collapseNavActive: !prevState.collapseNavActive };
        });
    }

    render() {
        return (
            <Aux>
                <Navigation
                    isAuthenticated={this.props.isAuthenticated}
                    collapseNavActive={this.state.collapseNavActive}
                    toggleMenu={this.toggleMenu}
                />
                <main className="container h-100">
                    {this.props.children}
                </main>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    }
};

export default connect(mapStateToProps)(Layout);