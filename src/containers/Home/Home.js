import React, { Component } from 'react';

import Modal from '../../components/UI/Modal/Modal';
import Backdrop from '../../components/UI/Backdrop/Backdrop';

class Home extends Component {
    state = {
        isOpen: false
    }

    clickHandler = () => {
        this.setState(prevState => {
            return { isOpen: !prevState.isOpen };
        });
    }

    render() {
        return (
            <div>
                <button onClick={this.clickHandler}>Click</button>
                <Backdrop show={this.state.isOpen} clicked={this.clickHandler}/>
                <Modal
                    title='Some title'
                    isOpen={this.state.isOpen}
                    submitted={this.clickHandler}
                    canceled={this.clickHandler}>
                    <div className="modal-header">
                        <h5 className="modal-title">Title</h5>
                        <button className="close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        ajajajaja
                    </div>
                    <div className="modal-footer">
                        <button className="btn btn-secondary">Close</button>
                        <button className="btn btn-primary">Save changes</button>
                    </div>
                </Modal>
            </div>
        );
    }
}

export default Home;