export const generateUniqueId = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var randomId = Math.random()* new Date().getTime();
        return randomId.toString(16);
    });
}