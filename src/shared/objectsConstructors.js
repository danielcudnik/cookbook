import { generateUniqueId } from './util';

export class Ingredient {
    constructor(name, quantity, quantityType, calories) {
        this.id = generateUniqueId();
        this.name = name;
        this.quantity = quantity;
        this.quantityType = quantityType;
        this.calories = calories;
    }
};

export class Recipe {
    constructor(title, ingredients, directions, notes, calories, time) {
        this.id = generateUniqueId();
        this.title = title;
        this.ingredients = ingredients;
        this.directions = directions;
        this.notes = notes;
        this.calories = calories;
        this.time = time;
    }
};